/**
 * Created by Young on 19/8/19.
 */
public class math{


	public static void main(String[] args){
		try {
			double tollerance = 0.01; //this seems to be six iterations
			double[] resultarr = bensBisectionMethod.bisection_iterate(0, 1, tollerance);
			int counter = 0;
			while(resultarr[1] != -999999 && counter < 6){
				resultarr = bensBisectionMethod.bisection_iterate(resultarr[0],resultarr[1],tollerance);
				counter++;
			}
			counter = 0;
			int maxIter = 6;

			resultarr = falsepos.falsepos_iteration(0,1,tollerance,counter,maxIter);
			while(resultarr[1] != -999999 && counter < maxIter){
				resultarr = falsepos.falsepos_iteration(resultarr[0],resultarr[1],tollerance,counter,maxIter);
				counter++;
			}

			counter = 0;
			double x = 0;
			while (counter < maxIter){

				x = newtons.newtoniterate(x,counter++);
			}

			counter = 0;
			x = 1;
			double y = 0;
			double holdx = x;

			while (counter<maxIter){
				x = secant.iterate(x,y,counter++);
				y = holdx;
				holdx = x;

			}


		}
		catch (Exception e){
			System.out.print(e.getMessage());
		}
		/*
		* Question B
		* The algorithim that is converging quicker to a solution is the bisection method
		* this is because the false position algorithm has  one side convergence for this function
		*
		*
		* */



	}


}