/**
 * Created by Young on 19/8/19.
 */
public class falsepos {

	public static double function(double a){
		return Math.cosh(a) + Math.pow(a,4) + a - 2;
	}
	public static String str(double a){
		return Double.toString(a);
	}

	public static void print_data(double a_value,double b_value,double a,double b,double error){
		System.out.print("Interval = [" + str(a) + ", " + str(b)+ "]");
		System.out.print(" Values = [" + str(a_value) + ", " + str(b_value) + "]");
		System.out.print(" error = " + str(error) + "\n");
	}

	public static boolean equal_up_to_tollerance(double y,double x,double tollerance){
		return Math.abs(y-x) < tollerance;
	}

	public static double[] falsepos_iteration(double a, double b,double tollerance,int counter,int max_iter) throws Exception{
		double a_value = function(a);
		if (/*equal_up_to_tollerance(a_value,0,tollerance) ||*/ counter == max_iter-1 ) {
			System.out.print("Root is = " + str(a) + "\n");
			double[] returnval = {a_value,-999999};
			return returnval;
		}
		double b_value = function(b);
		if(/*equal_up_to_tollerance(b_value,0,tollerance)*/ counter == max_iter-1){
			System.out.print("Root is = " + str(b) + "\n");
			double[] returnval = {b_value,-999999};
			return returnval;
		}
		double mid = ((function(a)*b) - (function(b) * a))/(function(a)-function(b));
		double mid_value = function(mid);
		if (/*equal_up_to_tollerance(mid_value,0,tollerance)*/ counter == max_iter-1){
			System.out.print("Root is = " + str(mid) + "\n");
			double[] returnval = {mid_value,-999999};
			return returnval;
		}

		double product = a_value * b_value;

		if (product > 0) {
			throw new Exception("No root in interval");
		}
		print_data(a_value,b_value,a,b,b-a);
		if(a_value * mid_value < 0){
			double[] returnval = {a,mid};
			return returnval;
		}
		double[] returnval = {mid,b};
		return returnval;
	}

}
// java program for implementation
// of Bisection Method for
// solving equations
/*
import java.io.*;


class falsepos {

	static int MAX_ITER = 6;

	// An example function whose
	// solution is determined using
	// Bisection Method. The function
	// is x^3 - x^2 + 2
	static double func(double a) {
		return (Math.cosh(a) + Math.pow(a,4) + a - 2);
	}

	// Prints root of func(x)
	// in interval [a, b]
	public static void regulaFalsi(double a, double b) {
		if (func(a) * func(b) >= 0) {
			System.out.println("You have not assumed right a and b");
		}
		// Initialize result
		double c = a;

		for (int i = 0; i < MAX_ITER; i++) {
			// Find the point that touches x axis
			c = (a * func(b) - b * func(a))
					/ (func(b) - func(a));

			// Check if the above found point is root
			if (func(c) == 0)
				break;

				// Decide the side to repeat the steps
			else if (func(c) * func(a) < 0)
				b = c;
			else
				a = c;
		}
		System.out.println("The value of root is : " + c);
	}
} */