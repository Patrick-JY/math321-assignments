/**
 * Created by Young on 19/8/19.
 */
public class bensBisectionMethod {
	//code taken from notes with minor alterations
	public static double function(double a){
		return Math.cosh(a) + Math.pow(a,4) + a - 2;
	}

	public static String str(double a){
		return Double.toString(a);
	}

	public static void print_data(double a_value,double b_value,double a,double b,double error){
		System.out.print("Interval = [" + str(a) + ", " + str(b)+ "]");
		System.out.print(" Values = [" + str(a_value) + ", " + str(b_value) + "]");
		System.out.print(" error = " + str(error) + "\n");
	}

	public static boolean equal_up_to_tollerance(double y,double x,double tollerance){
		return Math.abs(y-x) < tollerance;
	}

	public static double[] bisection_iterate(double a, double b,double tollerance) throws Exception{
		double a_value = function(a);
		if (equal_up_to_tollerance(a_value,0,tollerance)) {
			System.out.print("Root is = " + str(a) + "\n");
			double[] returnval = {a_value,-999999};
			return returnval;
		}
		double b_value = function(b);
		if(equal_up_to_tollerance(b_value,0,tollerance)){
			System.out.print("Root is = " + str(b) + "\n");
			double[] returnval = {b_value,-999999};
			return returnval;
		}
		double mid = 0.5 * (a+b);
		double mid_value = function(mid);
		if (equal_up_to_tollerance(mid_value,0,tollerance)){
			System.out.print("Root is = " + str(mid) + "\n");
			double[] returnval = {mid_value,-999999};
			return returnval;
		}

		double product = a_value * b_value;

		if (product > 0) {
			throw new Exception("No root in interval");
		}
		print_data(a_value,b_value,a,b,b-a);
		if(a_value * mid_value < 0){
			double[] returnval = {a,mid};
			return returnval;
		}
		double[] returnval = {mid,b};
		return returnval;
	}
}
