/**
 * Created by Young on 19/8/19.
 */
public class secant {
	public static double function(double a){
		return Math.cosh(a) + Math.pow(a,4) + a - 2;
	}
	public static double iterate(double x,double y,int counter){
		x = y - function(y)* ((x-y)/(function(x) - function(y)));
		System.out.println("x_" + counter + "= " + x);
		return x;
	}
}
